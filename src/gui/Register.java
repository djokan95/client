package gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

import logic.ErrorDialog;
import logic.User;


public class Register extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	JPasswordField password;
	JTextField username,email;
	public Register()
	{
		super("Register!");
		setLayout(new GridLayout(5,1));
		addWindowListener(
				new WindowAdapter()
				{
					public void windowClosing(WindowEvent a)
					{
						dispose();
					}
				}
				);
		
		JPanel p1= new JPanel();
		JPanel p11=new JPanel();
		JLabel usernamelabel= new JLabel("Username:");
		
		p11.add(usernamelabel);
		p1.add(p11);
		JPanel p12= new JPanel();
		username=new JTextField();
		username.setPreferredSize(new Dimension(100, 20));
		p12.add(username);
		
		p1.add(p12);
		add(p1);
		
		JPanel p2= new JPanel();
		JPanel p21= new JPanel();
		JLabel passwordlabel= new JLabel("Password:");
		p21.add(passwordlabel);
		p2.add(p21);
		JPanel p22= new JPanel();
		password=new JPasswordField();
		password.setPreferredSize(new Dimension(100, 20));
		p22.add(password);
		p2.add(p22);
		add(p2);
		
		JPanel p3= new JPanel();
		JPanel p31=new JPanel();
		JLabel emaillabel= new JLabel("Email:");
		
		p31.add(emaillabel);
		p3.add(p31);
		JPanel p32= new JPanel();
		email=new JTextField();
		email.setPreferredSize(new Dimension(100, 20));
		p32.add(email);
		
		p3.add(p32);
		add(p3);
		
		
		
		JPanel p4= new JPanel();
		p4.setLayout(new GridLayout(1,2));
		
		
		JPanel p41 = new JPanel();
		JButton login = new JButton("Register");
		
		login.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	try {
					new Session(new User(username.getText(),password.getPassword(),email.getText()));
					Register.this.dispose();
				} catch (Exception e1) {
					new ErrorDialog(Register.this,"Registration Error!");
					e1.printStackTrace();
				}
		    }
		});
		p41.add(login);
		p4.add(p41);
		add(p4);
		
		JPanel p5= new JPanel();
		p5.setLayout(new GridLayout(1,2));
		
		
		JPanel p51 = new JPanel();
		JButton reg = new JButton("Login");
		
		reg.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	new Login();
		    	Register.this.dispose();
		    }
		});
		p51.add(reg);
		p5.add(p51);
		add(p5);
		
		setResizable(false);
		setSize(450,350);
		logic.Utilities.centerScreen(this);
		setVisible(true);
		
	}
	
	
	
}
