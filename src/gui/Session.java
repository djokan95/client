package gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

import logic.User;

public class Session extends JFrame{

	private static final long serialVersionUID = 1L;
	public String status;
	public Session(User user) {
		super("Session!");
		addWindowListener(
				new WindowAdapter()
				{
					public void windowClosing(WindowEvent a)
					{
						dispose();
					}
				}
				);
		
		
		
		
		
		
		setResizable(false);
		setSize(450,350);
		logic.Utilities.centerScreen(this);
		setVisible(true);
	}

}
