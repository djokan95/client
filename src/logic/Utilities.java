package logic;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JDialog;
import javax.swing.JFrame;


public class Utilities {
	public static void centerScreen(JFrame a)
	{
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		a.setLocation(dim.width/2-a.getWidth()/2, dim.height/2-a.getHeight()/2);
		
	}
	public static void centerScreen(JDialog a)
	{
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		a.setLocation(dim.width/2-a.getWidth()/2, dim.height/2-a.getHeight()/2);
		
	}
	public static PrivateKey DecryptPrivateKey(PrivateKey enc,char[] password) throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException
	{
		KeyFactory kf = KeyFactory.getInstance("RSA");
		
		return kf.generatePrivate(new RSAPrivateKeySpec(   ((RSAPrivateKey)enc).getModulus(),new BigInteger(Utilities.AesDecrypt(((RSAPrivateKey)enc).getPrivateExponent().toByteArray(),password))    ));
		
		
		
	}
	
	public static PrivateKey DecryptPrivateKey(byte[] mod, byte[] exp,char[] password) throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException
	{
		KeyFactory kf = KeyFactory.getInstance("RSA");
		
		
		return kf.generatePrivate(new RSAPrivateKeySpec(   new BigInteger(mod),new BigInteger(Utilities.AesDecrypt(exp,password)) ));
		
		
	}
	
	public static byte[] sha1(byte[] a) throws NoSuchAlgorithmException
	{
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		return md.digest(a);
	}
	
	public static PublicKey MakePublicKey(byte[] mod) throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException
	{
		KeyFactory kf = KeyFactory.getInstance("RSA");
		
		
		return kf.generatePublic(new RSAPublicKeySpec(   new BigInteger(mod),new BigInteger("65537") ));
		
		
	}
	
	
	public static PrivateKey EncryptPrivateKey(PrivateKey p,char[] password) throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException
	{
		KeyFactory kf = KeyFactory.getInstance("RSA");
		byte[] cryptedexponent=Utilities.AesEncrypt(((RSAPrivateKey)p).getPrivateExponent().toByteArray(),password);
		
		return kf.generatePrivate(new RSAPrivateKeySpec(  ((RSAPrivateKey)p).getModulus(),new BigInteger(cryptedexponent)));
		
	}
	public static void printnum(byte[] a)
	{
		for (int i=0;i<a.length;i++)
		{
			System.out.print((int)a[i]+" ");
		}
		System.out.println();
	}
	
	public static byte[] copy(byte[] one,int a, int b)
	{
		byte[] combined = new byte[b-a];

		System.arraycopy(one,a,combined,0         ,b-a);
		
		return combined;
	}
	
	public static byte[] add(byte[] one,byte[] two)
	{
		byte[] combined = new byte[one.length + two.length];

		System.arraycopy(one,0,combined,0         ,one.length);
		System.arraycopy(two,0,combined,one.length,two.length);
		return combined;
	}
	public static SecretKey StringtoSecretKey(char[] s) throws NoSuchAlgorithmException
	{
		
		byte[] decodedKey = Charset.forName("UTF-8").encode(CharBuffer.wrap(s)).array();
		MessageDigest sha = MessageDigest.getInstance("SHA-1");
		decodedKey = sha.digest(decodedKey);
		decodedKey = Arrays.copyOf(decodedKey, 16);
		SecretKey sec=new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES"); 
		for (int i=0;i<decodedKey.length;i++)
		decodedKey[i]=0x00;
		return sec;
	}
	
	public static byte[] AesDecrypt(byte[] chipertext,char[] key) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException
	{
		Cipher c1 = Cipher.getInstance("AES");
		SecretKey s=StringtoSecretKey(key);
		
		c1.init(Cipher.DECRYPT_MODE, s);
		byte[] plaintext;
		if (chipertext.length%16==1) plaintext=c1.doFinal(Arrays.copyOfRange(chipertext,1,chipertext.length));
		else {
			plaintext=c1.doFinal(chipertext);
			
		}
		try{
			//s.destroy();
		}catch(Exception e){}
		return plaintext;
	}
	
	public static byte[] AesEncrypt(byte[] plaintext,char[] key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
	{	
		Cipher c1 = Cipher.getInstance("AES");
		SecretKey s=StringtoSecretKey(key);
		c1.init(Cipher.ENCRYPT_MODE, s);
		byte[] chipertext=c1.doFinal(plaintext);
		try{
			
		}catch(Exception e){}
		return chipertext;
	}
	
	public static void SaveKeyPair(String path, KeyPair keyPair, char[] password) throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException {
		PrivateKey privateKey = keyPair.getPrivate();
		privateKey= EncryptPrivateKey(privateKey,password);

		
		PublicKey publicKey = keyPair.getPublic();
 
		// Store Public Key.
		X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(
				publicKey.getEncoded());
		File file = new File(path + "/public.key");
		
		if (!file.exists())
		{
			file.getParentFile().mkdirs(); 
			file.createNewFile();
		}
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(x509EncodedKeySpec.getEncoded());
		fos.close();
 
		// Store Private Key.
		PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(
				privateKey.getEncoded());
		fos = new FileOutputStream(path + "/private.key");
		fos.write(pkcs8EncodedKeySpec.getEncoded());
		fos.close();
	}
 
	public static KeyPair LoadKeyPair(String path, char[] password)
			throws IOException, NoSuchAlgorithmException,
			InvalidKeySpecException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		// Read Public Key.
		File filePublicKey = new File(path + "/public.key");
		FileInputStream fis = new FileInputStream(path + "/public.key");
		byte[] encodedPublicKey = new byte[(int) filePublicKey.length()];
		fis.read(encodedPublicKey);
		fis.close();
 
		// Read Private Key.
		File filePrivateKey = new File(path + "/private.key");
		fis = new FileInputStream(path + "/private.key");
		byte[] encodedPrivateKey = new byte[(int) filePrivateKey.length()];
		fis.read(encodedPrivateKey);
		fis.close();
 
		// Generate KeyPair.
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(
				encodedPublicKey);
		PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
 
		PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(
				encodedPrivateKey);
		PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
		
		privateKey= DecryptPrivateKey(privateKey,password);
		
		return new KeyPair(publicKey, privateKey);
	}
	
	public static KeyPair LoadCleanKeyPair(String path, char[] password)
			throws IOException, NoSuchAlgorithmException,
			InvalidKeySpecException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		// Read Public Key.
		File filePublicKey = new File(path + "/public.key");
		FileInputStream fis = new FileInputStream(path + "/public.key");
		byte[] encodedPublicKey = new byte[(int) filePublicKey.length()];
		fis.read(encodedPublicKey);
		fis.close();
 
		// Read Private Key.
		File filePrivateKey = new File(path + "/private.key");
		fis = new FileInputStream(path + "/private.key");
		byte[] encodedPrivateKey = new byte[(int) filePrivateKey.length()];
		fis.read(encodedPrivateKey);
		fis.close();
 
		// Generate KeyPair.
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(
				encodedPublicKey);
		PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
 
		PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(
				encodedPrivateKey);
		PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
		
		
		return new KeyPair(publicKey, privateKey);
	}
	
	public static BigInteger modulus(PublicKey p)
	{
		return ((RSAPublicKey)p).getModulus();
		
	}
	
	public static BigInteger modulus(PrivateKey p)
	{
		return ((RSAPrivateKey)p).getModulus();
		
	}
	
	public static BigInteger exponent(PrivateKey p)
	{
		return ((RSAPrivateKey)p).getPrivateExponent();
		
	}
	
	public static boolean isValid(KeyPair k)
	{
		String s="test";
		try {
		Cipher c1 = Cipher.getInstance("RSA");
		c1.init(Cipher.ENCRYPT_MODE, k.getPrivate());
		Cipher c2 = Cipher.getInstance("RSA");
		c2.init(Cipher.DECRYPT_MODE, k.getPublic());
		byte[] chipertext=c1.doFinal(s.getBytes());
		byte[] plaintext=c2.doFinal(chipertext);
		if (new String(plaintext).equals("test")) return true;
		}catch(Exception e) {}
		return false;
	}
	
}
