package logic;

import java.io.File;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.security.*;
import java.util.Arrays;


public class User {

	private String username;
	private char[] password;
	private KeyPair keys;
	public User(String username,char[] password) throws Exception
	{
		this.username=username;
		this.password=password;
		File FS= new File("FS");
		if (!(FS.exists()))
		{

		    FS.mkdir();
		    
		};
		File userdir= new File("FS/"+ username);
		if (!(userdir.exists()))
		{

			userdir.mkdir();
		    
		};
		boolean valid=true;
		try {
			keys=Utilities.LoadCleanKeyPair("FS/"+ username + "/keys",password);
		} catch (Exception e) {
			valid=false;
			
		}
		if (valid)
		{
			
			DatagramSocket a= new DatagramSocket();
			byte[] temp= ("l"+username+'\n').getBytes();
			DatagramPacket req = new DatagramPacket(temp,temp.length,InetAddress.getByName("198.199.126.244"),23821);
			a.send(req);
			DatagramPacket res = new DatagramPacket(new byte[1024],1024);
			a.receive(res);
			a.close();
			temp=res.getData();
				
			if (((char)temp[0])=='s')
			{
				System.out.println("evo");
				byte[] b= Utilities.copy(temp,1, 21);
				if (!Arrays.equals(b,Utilities.sha1(Utilities.add(Utilities.modulus(keys.getPrivate()).toByteArray(), Utilities.exponent(keys.getPrivate()).toByteArray())) ))
				{
					Utilities.printnum(b);
					Utilities.printnum(Utilities.sha1(Utilities.add(Utilities.modulus(keys.getPrivate()).toByteArray(), Utilities.exponent(keys.getPrivate()).toByteArray())));
					refreshkeypair(username,password);
				}
				else
				{
					keys=new KeyPair(keys.getPublic(),Utilities.DecryptPrivateKey(keys.getPrivate(), password));
					if (!Utilities.isValid(keys)) 
						throw new Exception();
						
				}
						
			} 
			else 
			{
				throw new Exception();
			}
			
			
			
			
			
		}
		else 
		{
			refreshkeypair(username,password);
		}
		
		
	}
	private void refreshkeypair(String username,char[] password) throws Exception
	{
		System.out.print("refreshkeypair");
		this.username=username;
		this.password=password;
		File priv= new File("FS/"+ username + "/keys/private.key");
		File publ= new File("FS/"+ username + "/keys/public.key");
		if (priv.exists()) priv.delete();
		if (publ.exists()) publ.delete();
		DatagramSocket a= new DatagramSocket();
		byte[] temp=("i"+username+"\n").getBytes();
		DatagramPacket req = new DatagramPacket(temp,temp.length,InetAddress.getByName("198.199.126.244"),23821);
		a.send(req);
		DatagramPacket res = new DatagramPacket(new byte[1024],1024);
		a.receive(res);
		a.close();
		temp=res.getData();
		System.out.println(new String(temp));
		if (((char)temp[0])=='s')
		{
			int modlen=temp[1]*256+temp[2];
			int explen=temp[3]*256+temp[4];
			byte[] mod=Utilities.copy(temp, 5, 5+modlen);
			byte[] exp=Utilities.copy(temp, 5+modlen, 5+modlen+explen);
			
			PrivateKey pr=Utilities.DecryptPrivateKey(mod, exp, password);
			PublicKey pub=Utilities.MakePublicKey(mod);
			
			KeyPair kp= new KeyPair(pub,pr);
			
			if (Utilities.isValid(kp))
			{
				Utilities.SaveKeyPair("FS/"+ username + "/keys", kp, password);
			}
			else
			{
				throw new Exception();
			}
		}
		else
		{
			throw new Exception();
		}
	}
	
	
	public User(String username, char[] password, String email) throws Exception  {
		File FS= new File("FS");
		if (!(FS.exists()))
		{

		    FS.mkdir();
		    System.out.println("DIR created");
		    
		};
		File userdir= new File("FS/"+ username);
		if (!(userdir.exists()))
		{

			userdir.mkdir();
		    System.out.println("DIR created"); 
		    
		};
		KeyPairGenerator keyGen=null;
		try {
			keyGen = KeyPairGenerator.getInstance("RSA");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		keyGen.initialize(2048, new SecureRandom());
		KeyPair pair= keyGen.genKeyPair();
		
		DatagramSocket a= new DatagramSocket();
		byte[] pub=Utilities.modulus(pair.getPublic()).toByteArray();
		byte[] priv=Utilities.exponent(Utilities.EncryptPrivateKey(pair.getPrivate(), password)).toByteArray();
		String temp= "r"+username+"\n"+email+"\n"+((char)(pub.length/256))+((char)(pub.length%256))+((char)(priv.length/256))+((char)(priv.length%256));
		//+new String(((RSAPublicKey)pair.getPublic()).getModulus().toByteArray())+new String(((RSAPrivateKey)Utilities.EncryptPrivateKey(pair.getPrivate(), password)).getModulus().toByteArray());
		byte[] b1=Utilities.add(temp.getBytes(),Utilities.add(pub,priv ));
		DatagramPacket req = new DatagramPacket(b1,b1.length,InetAddress.getByName("198.199.126.244"),23821);
		a.send(req);
		DatagramPacket res = new DatagramPacket(new byte[1024],1024);
		a.receive(res);
		a.close();
		String data= new String(res.getData());
		if (data.charAt(0)=='s')
		{
			Utilities.SaveKeyPair("FS/"+ username + "/keys", pair, password);
		}
		else
		{
			throw new Exception();
		}
		
	}
	public KeyPair getkeys()
	{
		return keys;
	}
	
	
}
