package app;

import gui.Login;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import javax.swing.JFrame;

import logic.ErrorDialog;


public class Client {
	
	public static void holepunching()
	{
		try(
				DatagramSocket a= new DatagramSocket();
				)
		{
			DatagramPacket req = new DatagramPacket("g".getBytes(),"g".length(),InetAddress.getByName("198.199.126.244"),23821);
			//DatagramPacket req = new DatagramPacket("g".getBytes(),"g".length(),InetAddress.getByName("192.168.0.100"),23821);
			a.send(req);
			a.setSoTimeout(5000);
			DatagramPacket res = new DatagramPacket(new byte[1024],1024);
			a.receive(res);
			String data=new String(res.getData());
			System.out.println(data);
			if (data.charAt(0)=='s')
			{
				res = new DatagramPacket(new byte[1024],1024);
				a.receive(res);
				data=new String(res.getData());
				req = new DatagramPacket("a".getBytes(),"a".length(),InetAddress.getByName(data.substring(1,data.indexOf('\n'))),new Integer(data.substring(data.indexOf('\n')+1,data.indexOf('\n',data.indexOf('\n')+1))));
				a.send(req);
				
				res = new DatagramPacket(new byte[1024],1024);
				a.receive(res);
				new ErrorDialog((JFrame)null,"server:"+ new String(res.getData()).substring(0,10));
			}
			else
			{
				if (data.charAt(0)=='a')
				{
					res = new DatagramPacket(new byte[1024],1024);
					a.receive(res);
					data=new String(res.getData());
				}
				System.out.println(new String(res.getData()).substring(0,30));
				req = new DatagramPacket("dosta".getBytes(),"dosta".length(),InetAddress.getByName(data.substring(1,data.indexOf('\n'))),new Integer(data.substring(data.indexOf('\n')+1,data.indexOf('\n',data.indexOf('\n')+1))));
				a.send(req);
				new ErrorDialog((JFrame)null,"client: sent!");
			}
		}
		catch (Exception e)
		{
			new ErrorDialog((JFrame)null,"Fail!");
			e.printStackTrace();
		}
	}
	
	public static void test()
	{
		try(
				DatagramSocket a= new DatagramSocket();
				)
		{
			DatagramPacket req = new DatagramPacket("g".getBytes(),"g".length(),InetAddress.getByName("198.199.126.244"),23821);
			//DatagramPacket req = new DatagramPacket("g".getBytes(),"g".length(),InetAddress.getByName("192.168.0.100"),23821);
			a.send(req);
			
			DatagramPacket res = new DatagramPacket(new byte[1024],1024);
			a.receive(res);
			String data=new String(res.getData());
			System.out.println(data);
			req = new DatagramPacket("g".getBytes(),"g".length(),InetAddress.getByName("198.199.126.244"),23821);
			//DatagramPacket req = new DatagramPacket("g".getBytes(),"g".length(),InetAddress.getByName("192.168.0.100"),23821);
			a.send(req);
			
			res = new DatagramPacket(new byte[1024],1024);
			a.receive(res);
			data=new String(res.getData());
			System.out.println(data);
		}
		catch (Exception e)
		{
			new ErrorDialog((JFrame)null,"Fail!");
			e.printStackTrace();
		}
	}
	public static logic.User user;
	
	public static void main(String argc[])
	{
		user=null;
		new Login();
	}
}
